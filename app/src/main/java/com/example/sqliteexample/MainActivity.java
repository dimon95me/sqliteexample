package com.example.sqliteexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_edit_text)
    EditText mainText;
//    @BindView(R.id.main_action_add)
//    Button actionAdd;
//    @BindView(R.id.main_action_view_data)
//    Button actionViewData;

//    EditText mainText;

    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//        mainText.findViewById(R.id.main_edit_text);

        databaseHelper = new DatabaseHelper(this);

    }

    @OnClick(R.id.main_action_add)
    public void actionAddOnClick() {

        if (mainText.getText().toString().length() != 0) {
            boolean insertData = databaseHelper.addData(mainText.getText().toString());
            if (insertData) {
                Toast.makeText(this, "Data added", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Data not added", Toast.LENGTH_SHORT).show();
            }
            mainText.setText("");
        } else {
            Toast.makeText(this, "Please, enter text first!", Toast.LENGTH_SHORT).show();
        }


    }

    @OnClick(R.id.main_action_view_data)
    public void actionViewDataOnClick() {

        Intent intent = new Intent(this, DataViewActivity.class);
        startActivity(intent);

    }

}
