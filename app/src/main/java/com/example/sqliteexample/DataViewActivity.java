package com.example.sqliteexample;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataViewActivity extends AppCompatActivity {

    private static final String TAG = "DataViewActivity";

    DatabaseHelper databaseHelper = new DatabaseHelper(this);
    @BindView
   (R.id.data_view_listview) ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_view);
        ButterKnife.bind(this);

        popualateListView();
    }

    private void popualateListView() {
        Log.d(TAG, "popualateListView: Displaying data in ListView");
        Cursor cursor = databaseHelper.getData();
        ArrayList<String> listStrings = new ArrayList<>();
        while (cursor.moveToNext()){
            listStrings.add(cursor.getString(1));
        }
        ListAdapter listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listStrings);
        listView.setAdapter(listAdapter);
    }
}
